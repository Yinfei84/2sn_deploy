#!/bin/sh

echo '╔═══════════════════════════════════════════════════════╗'
echo '║            AWESOME 2SN DEPLOY SCRIPT v0.1             ║'
echo '╚═══════════════════════════════════════════════════════╝'
echo ''
echo '                                                         '
echo '                 ▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄                    '
echo '               ▄▀            ▄       ▀▄                  '
echo '               █  ▄    ▄              █                  '
echo '               █            ▄█▄▄  ▄   █ ▄▄▄              '
echo '        ▄▄▄▄▄  █      ▀    ▀█  ▀▄     █▀▀ ██             '
echo '        ██▄▀██▄█   ▄       ██    ▀▀▀▀▀    ██             '
echo '         ▀██▄▀██        ▀ ██▀             ▀██            '
echo '           ▀████ ▀    ▄   ██   ▄█    ▄ ▄█  ██            '
echo '              ▀█    ▄     ██    ▄   ▄  ▄   ██            '
echo '              ▄█▄           ▀▄  ▀▀▀▀▀▀▀▀  ▄▀             '
echo '             █▀▀█████████▀▀▀▀████████████▀               '
echo '             ████▀  ███▀      ▀███  ▀██▀                 '
echo '                                                         '

sleep 2

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║             Installing VM needed services.            ║'
echo '╚═══════════════════════════════════════════════════════╝'

apt-get update
apt-get upgrade
apt-get install curl libc6 libcurl3 zlib1g
apt-get install php5 apache2 mysql-server libapache2-mod-php5 php5-mysql

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║                  Starting app setup                   ║'
echo '╚═══════════════════════════════════════════════════════╝'
echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║             Downloading app repository...             ║'
echo '╚═══════════════════════════════════════════════════════╝'
echo ''

curl -sL https://api.github.com/repos/Yinfei/2SN_Merge/tarball/master > tarball.tar

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║     Moving unzipped respository to root directory.    ║'
echo '╚═══════════════════════════════════════════════════════╝'

tar -xvf tarball.tar
cd Yinfei-2SN_Merge-314122f
sudo mv * ..
cd ..
rm -r Yinfei-2SN_Merge-314122f
rm tarball.tar
sudo chmod -R 777 app

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║                  Database migration.                  ║'
echo '╚═══════════════════════════════════════════════════════╝'

mysql -u root --password=totobolo -e "create database 2SN;"

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║             Installing Symfony2 services.             ║'
echo '╚═══════════════════════════════════════════════════════╝'

curl -sS https://getcomposer.org/installer | php

sudo rm -rf app/cache/prod/*
sudo rm -rf app/cache/dev/*
php app/console cache:clear --env=prod
sudo php composer.phar install
php app/console cache:clear --env=prod
php app/console doctrine:schema:update --force
php app/console cache:clear --env=prod

sudo chmod -R 777 app

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║             Adding sql dump to database.              ║'
echo '╚═══════════════════════════════════════════════════════╝'

mysql -u root --password=totobolo 2SN < dump.sql
php app/console doctrine:schema:update --force
rm dump.sql

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║             Adding app to enabled sites.              ║'
echo '╚═══════════════════════════════════════════════════════╝'

rm /etc/apache2/sites-available/default
sudo mv default /etc/apache2/sites-available/default
sudo a2ensite default

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║   Adding "authorization" header to accepted headers.  ║'
echo '╚═══════════════════════════════════════════════════════╝'

sudo a2enmod rewrite
sudo ls -al /etc/apache2/mods-enabled/rewrite.load
sudo service apache2 restart
sudo chmod -R 777 web
sudo chmod -R 777 app/cache

echo ''
echo '╔═══════════════════════════════════════════════════════╗'
echo '║               Installation Complete.                  ║'
echo '╚═══════════════════════════════════════════════════════╝'

echo ''
echo "                Y.                      _"
echo "                YiL                   .' '."
echo "                Yii;                .; .;; ."
echo "                YY;ii._           .; .;;;; :"
echo "                iiYYYYYYiiiii;;;;i  ;;::;;;;           such install"
echo "            _.;YYYYYYiiiiiiYYYii  .;;.   ;;;"
echo "         .YYYYYYYYYYiiYYYYYYYYYYYYii;   ;;;;"
echo "       .YYYYYYYSSYYiiYYSSSSiiiYYYYYY;.ii; .."
echo "      :YYYS!.  TYiiYYSSSSSYYYYYYYiiYYYYiYYii.       very finish"
echo "      YSMMS:   :YYYYYYS!'''4YYYYYiiiYYYYiiYY."
echo "   '. :MMSSb.,dYYSSYii' :    :YYYYllYiiYYYiYY"
echo "_.._ : 4MMS!YYYYYYYYYii,.__.diiiSSYYYYYYYYYYY"
echo ".,._ Sb'P'     '4SSSSSiiiiiiiiSSSSYYSSSSSSYiY;         much Symfony"
echo "    ,.S:'       :SSSSSSSSSYYYYYSSSSSSSSSYYiiYYL"
echo "    ''';SS   .;PPbS' ,.''TSSYYSSSSYYYYYYiiiYYU:"
echo "    ';SPS;;: ;;;;iSyS!YSSSb;SSSYSYYSSYYYiiiYYiYY"
echo "    SFiSS .. ':iii.'-:YYYYYSSYYSSSSSYYYiiYiYYY      wow"
echo "    :YSSrb ''  _..;;i;YYYSYYSSSSSSSYYYYYYYiYY:"
echo "     :SSSSSi;;iiiiidYYYYYYYYYYSSSSSSYYYYYYYiiYYYY."
echo "       SSSSSSSYYYYYYYYYYYYYSSSSSSYYYYYYYYiiiYYYYYY"
echo "      .i!SSSSSSYYYYYYYYYSSSSSSYYYSSYYiiiiiiYYYYYYY"
echo "     :YYiiiSSSSSSSYYYYYYYSSSSYYSSSSYYiiiiiYYYYYYi"
