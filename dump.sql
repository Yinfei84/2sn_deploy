-- MySQL dump 10.13  Distrib 5.6.17, for osx10.9 (x86_64)
--
-- Host: localhost    Database: 2SN
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `2SN`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `2SN` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `2SN`;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_39986E43A76ED395` (`user_id`),
  CONSTRAINT `FK_39986E43A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,1,'Wall','2014-12-17 19:25:57','2014-12-17 19:25:57'),(2,1,'Profile','2014-12-17 19:25:57','2014-12-17 19:25:57'),(3,2,'Wall','2014-12-17 19:26:42','2014-12-17 19:26:42'),(4,2,'Profile','2014-12-17 19:26:42','2014-12-17 19:26:42'),(5,3,'Wall','2014-12-17 19:27:11','2014-12-17 19:27:11'),(6,3,'Profile','2014-12-17 19:27:11','2014-12-17 19:27:11'),(7,4,'Wall','2014-12-17 19:27:39','2014-12-17 19:27:39'),(8,4,'Profile','2014-12-17 19:27:39','2014-12-17 19:27:39'),(9,5,'Wall','2014-12-17 21:45:47','2014-12-17 21:45:47'),(10,5,'Profile','2014-12-17 21:45:47','2014-12-17 21:45:47'),(11,6,'Wall','2014-12-17 21:46:20','2014-12-17 21:46:20'),(12,6,'Profile','2014-12-17 21:46:20','2014-12-17 21:46:20'),(13,7,'Wall','2014-12-17 21:46:59','2014-12-17 21:46:59'),(14,7,'Profile','2014-12-17 21:46:59','2014-12-17 21:46:59'),(15,7,'POWWA','2014-12-17 23:13:04','2014-12-17 23:13:04');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_session_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A2F98E478FE32B32` (`game_session_id`),
  CONSTRAINT `FK_A2F98E478FE32B32` FOREIGN KEY (`game_session_id`) REFERENCES `game_session` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (1,1,'Etna-Aventures'),(2,1,'Etna-Aventures'),(3,NULL,'Private_Channel_antoine_yohan'),(4,NULL,'Private_Channel_antoine_vincent_thierry_kevin_beckett'),(5,2,'lorem ipsum'),(6,2,'lorem ipsum'),(7,2,'lorem ipsum'),(8,NULL,'Private_Channel_kevin_antoine');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel_user`
--

DROP TABLE IF EXISTS `channel_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel_user` (
  `channel_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`channel_id`,`user_id`),
  KEY `IDX_11C7753772F5A1AA` (`channel_id`),
  KEY `IDX_11C77537A76ED395` (`user_id`),
  CONSTRAINT `FK_11C77537A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_11C7753772F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel_user`
--

LOCK TABLES `channel_user` WRITE;
/*!40000 ALTER TABLE `channel_user` DISABLE KEYS */;
INSERT INTO `channel_user` VALUES (1,2),(1,5),(1,6),(2,2),(3,2),(3,3),(4,2),(4,4),(4,5),(4,6),(4,7),(5,4),(5,6),(5,7),(6,4),(7,4),(8,2),(8,6);
/*!40000 ALTER TABLE `channel_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character_sheet`
--

DROP TABLE IF EXISTS `character_sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character_sheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `background` longtext COLLATE utf8_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sheet_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_79FF7680A76ED395` (`user_id`),
  CONSTRAINT `FK_79FF7680A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character_sheet`
--

LOCK TABLES `character_sheet` WRITE;
/*!40000 ALTER TABLE `character_sheet` DISABLE KEYS */;
INSERT INTO `character_sheet` VALUES (1,2,'Paul-Henry','Ninja','Ninja a temps plein.\r\nIl aime les pommes et faire du tricot.','5491ecf8944aa_ninja.jpg','5491ecf894645_file.pdf','2014-12-17 21:52:08','2014-12-17 21:52:08'),(2,3,'Gandalf','Wizzard','no memory of when he was born. \r\n\r\nturn ons include romantic walks on the beach and killing orcs.','5491f18896ffd_wizzard.png','5491f18897391_file.pdf','2014-12-17 22:11:36','2014-12-17 22:11:36'),(3,4,'Hipster bob','Parisien','Not Mainstream.','5491f40cf08d9_hipster.jpg','5491f40cf0bff_file.pdf','2014-12-17 22:22:20','2014-12-17 22:22:20'),(4,5,'Raoul','Paysan','J\'élève des cochons.','5491f79230c59_paysan.jpg','5491f79230dfb_file.pdf','2014-12-17 22:37:22','2014-12-17 22:37:22'),(5,6,'Fourtwenty','Druide','J\'aime la nature. ','5491fa948a00f_druide.jpg','5491fa948a158_file.pdf','2014-12-17 22:50:12','2014-12-17 22:50:12'),(6,6,'Zion','Lion','born in 1989, he has dedicated his life to justice and finding the hunter that turned his mother into a coat','5491fb0231d37_lion.jpg','5491fb0231e85_file.pdf','2014-12-17 22:52:02','2014-12-17 22:52:02'),(7,7,'Jack','Leprauchaun','he has a lot of gold. No you cant have any.','5491ff38d156e_dancing.gif','5491ff38d185f_file.pdf','2014-12-17 23:10:00','2014-12-17 23:10:00');
/*!40000 ALTER TABLE `character_sheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CA76ED395` (`user_id`),
  KEY `IDX_9474526C4B89032C` (`post_id`),
  CONSTRAINT `FK_9474526C4B89032C` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_9474526CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,3,8,'Joyeux anniversaire !','2014-12-17 22:06:08','2014-12-17 22:06:08'),(2,5,8,'Champagne !','2014-12-17 22:46:37','2014-12-17 22:46:37');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friend`
--

DROP TABLE IF EXISTS `friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `friendgroup_id` int(11) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_55EEAC61BEA3FA31` (`friendgroup_id`),
  KEY `IDX_55EEAC616A5458E8` (`friend_id`),
  KEY `IDX_55EEAC61A76ED395` (`user_id`),
  CONSTRAINT `FK_55EEAC61A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_55EEAC616A5458E8` FOREIGN KEY (`friend_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_55EEAC61BEA3FA31` FOREIGN KEY (`friendgroup_id`) REFERENCES `friend_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friend`
--

LOCK TABLES `friend` WRITE;
/*!40000 ALTER TABLE `friend` DISABLE KEYS */;
INSERT INTO `friend` VALUES (1,3,2,3,'antoine'),(2,5,3,2,'antoine'),(3,3,2,4,'antoine'),(4,15,4,2,'antoine'),(5,3,2,6,'antoine'),(6,11,6,2,'antoine'),(7,5,3,7,'yohan'),(8,13,7,3,'yohan'),(9,7,4,7,'vincent'),(10,13,7,4,'vincent'),(11,7,4,5,'vincent'),(12,9,5,4,'vincent'),(13,7,4,6,'vincent'),(14,11,6,4,'vincent'),(15,10,5,2,'thierry'),(16,4,2,5,'thierry'),(17,11,6,7,'kevin'),(18,13,7,6,'kevin'),(19,12,6,3,'kevin'),(20,6,3,6,'kevin');
/*!40000 ALTER TABLE `friend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friend_groups`
--

DROP TABLE IF EXISTS `friend_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friend_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2176CE19A76ED395` (`user_id`),
  CONSTRAINT `FK_2176CE19A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friend_groups`
--

LOCK TABLES `friend_groups` WRITE;
/*!40000 ALTER TABLE `friend_groups` DISABLE KEYS */;
INSERT INTO `friend_groups` VALUES (1,1,'general'),(2,1,'wait'),(3,2,'general'),(4,2,'wait'),(5,3,'general'),(6,3,'wait'),(7,4,'general'),(8,4,'wait'),(9,5,'general'),(10,5,'wait'),(11,6,'general'),(12,6,'wait'),(13,7,'general'),(14,7,'wait'),(15,4,'ETNA');
/*!40000 ALTER TABLE `friend_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_session`
--

DROP TABLE IF EXISTS `game_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'My Game : Give me a name, please !',
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4586AAFBA76ED395` (`user_id`),
  CONSTRAINT `FK_4586AAFBA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_session`
--

LOCK TABLES `game_session` WRITE;
/*!40000 ALTER TABLE `game_session` DISABLE KEYS */;
INSERT INTO `game_session` VALUES (1,2,'Etna-Aventures','Environnement Donjon et Dragons.\n\nrecherche joueurs actifs (connexion 1 a 2 fois par jour minimum).'),(2,4,'lorem ipsum','petite game posé, pour les fans des aventures de kawaquitzy.\n\ntout le monde est bienvenu.');
/*!40000 ALTER TABLE `game_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_session_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ACB79A358FE32B32` (`game_session_id`),
  KEY `IDX_ACB79A359A4AA658` (`guest_id`),
  CONSTRAINT `FK_ACB79A359A4AA658` FOREIGN KEY (`guest_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ACB79A358FE32B32` FOREIGN KEY (`game_session_id`) REFERENCES `game_session` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest`
--

LOCK TABLES `guest` WRITE;
/*!40000 ALTER TABLE `guest` DISABLE KEYS */;
INSERT INTO `guest` VALUES (5,2,2),(6,2,3),(7,2,5);
/*!40000 ALTER TABLE `guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `contents` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6BD307F72F5A1AA` (`channel_id`),
  KEY `IDX_B6BD307FF624B39D` (`sender_id`),
  CONSTRAINT `FK_B6BD307FF624B39D` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B6BD307F72F5A1AA` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,3,2,'Yop ! comment ca va depuis le temps vieux ?!'),(2,3,3,'Oh ba la routine. Et toi alors ?'),(3,4,4,'Soirée hipster, va y avoir de la musique que personne connait #pasMainstream'),(4,5,4,'Bienvenue a tous, je serai votre master ! (POPOPO)'),(5,4,5,'Je viens seulement si tu nous refais ton fameux plat vietnamien avec les amandes. j\'ai A-DO-RÉ la dernière fois.'),(6,1,5,'Bonjour tout le monde :)'),(7,1,6,'bonsoir !'),(8,4,6,'Je serai pas dispo. une autre fois peut etre.'),(9,8,6,'Antoine ? Tu as deux secondes ?'),(10,4,7,'pas assez de SWAG pour moi.'),(11,5,7,'Bonjour. comment allez vous ?');
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_14B784181137ABCF` (`album_id`),
  CONSTRAINT `FK_14B784181137ABCF` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` VALUES (1,4,'','5491ee0b4174d_antoine.jpeg','2014-12-17 21:56:43','2014-12-17 21:56:43'),(2,3,'Mon animal préféré. <3 <3','5491efbcb5b15_poney-rose.jpg','2014-12-17 22:03:56','2014-12-17 22:03:56'),(3,6,'','5491efeee3f18_yohan.jpeg','2014-12-17 22:04:46','2014-12-17 22:04:46'),(4,5,'Ambiance posé.','5491f2bcad6ad_candle.jpg','2014-12-17 22:16:44','2014-12-17 22:16:44'),(5,5,'Mon chat d\'amour !','5491f2ff57fdd_mon_chat.jpg','2014-12-17 22:17:51','2014-12-17 22:17:51'),(6,8,'','5491f36fb326a_vincent.jpeg','2014-12-17 22:19:43','2014-12-17 22:19:43'),(7,7,'Escalope de salade. #sansGluten #Bio','5491f68f2f104_repas.jpg','2014-12-17 22:33:03','2014-12-17 22:33:03'),(8,10,'','5491f6c07286e_thierry.jpeg','2014-12-17 22:33:52','2014-12-17 22:33:52'),(9,9,'Mon athlete qui se donne a fond pour papa ! GO GO GO petit junior, c\'est toi le plus fort !','5491f9518240f_course_cochons.jpg','2014-12-17 22:44:49','2014-12-17 22:44:49'),(10,12,'','5491f9e8101c6_kevin.jpeg','2014-12-17 22:47:20','2014-12-17 22:47:20'),(11,11,'This is not just a drink. This is the BEST drink. this is ICE TEA.','5491fbdc33b0f_bestIceTea.jpg','2014-12-17 22:55:40','2014-12-17 22:55:40'),(13,14,'','5491fdd44cea1_beckett.jpeg','2014-12-17 23:04:04','2014-12-17 23:04:04'),(14,15,'AAAAAAAMEEEEERICA !','5492008cd3b83_AmericaBoy.png','2014-12-17 23:15:40','2014-12-17 23:15:40'),(15,15,'let me show you the world','549200a3be7f6_avatar.png','2014-12-17 23:16:03','2014-12-17 23:16:03'),(16,15,'2SN GROUP FOR EVER !','549200b62a3c7_avengers2SN.png','2014-12-17 23:16:22','2014-12-17 23:16:22'),(17,15,'COOOOOOOOOOKIE !','549200fc54d9d_cookieMonster.png','2014-12-17 23:17:32','2014-12-17 23:17:32'),(18,15,'LEQUEREC BROS','5492011847574_Cousins.png','2014-12-17 23:18:00','2014-12-17 23:18:00'),(19,15,'#vegan #NotMaintream #ebola','5492013d81c52_hipsterMax.png','2014-12-17 23:18:37','2014-12-17 23:18:37'),(20,15,'Where is the rat ?!','54920155af0cf_ninjaTurtles.png','2014-12-17 23:19:01','2014-12-17 23:19:01'),(21,15,'when loving a language goes too far ...','5492017fd45cb_rubyMan.png','2014-12-17 23:19:43','2014-12-17 23:19:43'),(22,15,'And IIIIIIIIIIIIIIIIII-y-AAAAAAAA WILL ALWAYS LOVE FOOOOOOOOD !','549201a2de8fe_titanic3D.png','2014-12-17 23:20:18','2014-12-17 23:20:18'),(23,15,'Like a Lion for Zion','549201b3a5afe_zionLion.png','2014-12-17 23:20:35','2014-12-17 23:20:35');
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `charactersheet_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_98197A658FE32B32` (`game_session_id`),
  KEY `IDX_98197A65A76ED395` (`user_id`),
  KEY `IDX_98197A6596FF67E5` (`charactersheet_id`),
  CONSTRAINT `FK_98197A6596FF67E5` FOREIGN KEY (`charactersheet_id`) REFERENCES `character_sheet` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_98197A658FE32B32` FOREIGN KEY (`game_session_id`) REFERENCES `game_session` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_98197A65A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
INSERT INTO `player` VALUES (1,1,5,4),(2,1,6,5),(3,2,6,6),(4,2,7,7);
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A8A6C8DA76ED395` (`user_id`),
  CONSTRAINT `FK_5A8A6C8DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,'Hey Welcome !','2014-12-17 19:25:57','2014-12-17 19:25:57',NULL),(2,2,'Hey Welcome !','2014-12-17 19:26:42','2014-12-17 19:26:42',NULL),(3,3,'Hey Welcome !','2014-12-17 19:27:11','2014-12-17 19:27:11',NULL),(4,4,'Hey Welcome !','2014-12-17 19:27:39','2014-12-17 19:27:39',NULL),(5,5,'Hey Welcome !','2014-12-17 21:45:47','2014-12-17 21:45:47',NULL),(6,6,'Hey Welcome !','2014-12-17 21:46:20','2014-12-17 21:46:20',NULL),(7,7,'Hey Welcome !','2014-12-17 21:46:59','2014-12-17 21:46:59',NULL),(8,2,'C\'est mon Anniversaire !','2014-12-17 21:54:20','2014-12-17 21:54:20',NULL),(9,4,'Découvert un nouvel album sympa de jeibshgbvkhjsbfsk, un artiste russe que personne connait. #JeSuisDifferent','2014-12-17 22:30:59','2014-12-17 22:30:59',NULL),(10,6,'Des gens cherchent un mec pour jouer support dans une game ?','2014-12-17 22:56:44','2014-12-17 22:56:44',NULL),(12,7,'@antoine tu es un peu nem-esis.','2014-12-17 23:23:06','2014-12-17 23:23:06',NULL);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `image_profile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Admin','admin','admin@etna-alternance.net','admin@etna-alternance.net',1,'flkwq35du9wgkw8c0scscskgowwk8kk','FeuhSvywir7cvdtEyQ3UcW26E6T1VZFkAhi8OmVO//LFBU3K+ubr9GqM04xCEY1+S7AcetIFi6XOfhPJf3XVVQ==','2014-12-17 19:25:57',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'anon_user.png'),(2,'antoine','antoine','thich_a@etna-alternance.net','thich_a@etna-alternance.net',1,'7y8taxej95og00gssgs84w4gswwssk0','AnzUab0+u59PqlSq/HrvfH+ZR9bA+SGWTutWKD4tohRXP4jwSa98+6HSXGVrn+/Pfv4Zay4hrMKQnb4BXcDNUw==','2014-12-17 23:46:21',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491ee0b4174d_antoine.jpeg'),(3,'yohan','yohan','piron_y@etna-alternance.net','piron_y@etna-alternance.net',1,'jxhxtthqo408gosk8sck8ocgw8o4oos','3fTgAD4UQzr7AubXUORPFxZMvv4hk/K1dxaAWaMvosPGNgYkORIUDu3HsgCnaUTqAxiEsbNpqg6IziWX6IwPCw==','2014-12-17 22:04:31',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491efeee3f18_yohan.jpeg'),(4,'vincent','vincent','quegui_v@etna-alternance.net','quegui_v@etna-alternance.net',1,'8mecb1oyfmw48k484cgcocgkso04o8g','BZD3ZkDqo9nCVUQ+i6/ovPMMYsT3ffPOxIrklCipFsXba+m3htePjfA5lEXaaCS7Fz3MFvRw3jpZOm1+uDzWxw==','2014-12-17 22:19:29',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491f36fb326a_vincent.jpeg'),(5,'thierry','thierry','barret_t@etna-alternance.net','barret_t@etna-alternance.net',1,'rkcax8fzo74wgc4c4okkkwo400koc8k','OcM0PqUYqCZ2VsTRMLD3cwNwb3+fzP4I9VZgFk5OLu1eiaMZ5IHYG9FsyhACU8TpBmIz9xPjpeThdALdTpR5qQ==','2014-12-17 22:33:39',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491f6c07286e_thierry.jpeg'),(6,'kevin','kevin','hoquet_k@etna-alternance.net','hoquet_k@etna-alternance.net',1,'jmsr6oxdndw040o8c40gs8o8gs40cck','TKL0ub2vhqNH7ZtWALTaDFVtoWicau42rG5sijzhOI7v3gTC6jpZFExXTPX2zuZYuHxiBDsaQwx6XT1gpMC3HA==','2014-12-17 22:46:58',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491f9e8101c6_kevin.jpeg'),(7,'beckett','beckett','hover_b@etna-alternance.net','hover_b@etna-alternance.net',1,'lwyhncltpdco0so8wwokoswk44w4g8c','xU35tIAsxbsGZWQKSfU7n+62nlBjQxO10isf5GRdKv5PSnlWKja8Drwf57X1ujEnuSS7DXPmWyUmryC7VB3kxw==','2014-12-17 23:02:09',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,NULL,'5491fdd44cea1_beckett.jpeg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-17 23:57:14
